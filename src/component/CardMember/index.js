import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity} from 'react-native';

const CardMember = ({id, anggota, navigation, removeData}) => {
        return (
            <TouchableOpacity style={styles.countainer}
            onPress={() => navigation.navigate('DetailMember', {id: id})}>
                <View>                    
                <Text style={styles.nama}>{anggota.nama} </Text>
                <Text style={styles.profesi}>Jabatan : {anggota.profesi} </Text>
                </View>
                <View style={styles.icon}>
                    <FontAwesomeIcon icon={faEdit} color={'orange'} size={25} 
                     onPress={() => navigation.replace('EditMember', {id: id})}/>
                    <FontAwesomeIcon icon={faTimes} color={'red'} size={25} 
                    onPress={() => removeData(id)} />
                </View>
            </TouchableOpacity>
        )
}
export default CardMember;

const styles = StyleSheet.create({
    countainer: {
        flexDirection: 'row',
        padding: 10,
        backgroundColor: 'white',
        borderRadius:5,
        marginBottom: 20,
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation: 5,
    },
    nama:{
        marginBottom:5,
        fontFamily:'times new roman',
        fontSize: 16,
        fontWeight:'bold',
    },
    profesi:{
        fontFamily:'times new roman',
        fontSize: 14,
    },
    icon:{
        flexDirection:'row',
        flex: 1,
        justifyContent: 'flex-end',
        alignItems:'center',
    }
})
