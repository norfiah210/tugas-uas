import React from 'react';
import {Text, TextInput, StyleSheet} from 'react-native';
  
const InputMember = ({label, placeholder, onChageText, value,namaState}) => {
        return (
        <> 
        <Text style={styles.label}>{label} </Text>
        <TextInput placeholder={placeholder}
        style={styles.inputarea}
        onChangeText={text => onChageText(namaState, text)} 
        value={value} />
        </>
    );
};
export default InputMember;

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        marginBottom:5,
    },
    inputarea: {
        textAlignVertical: 'top',
        borderBottomWidth: 1,
        borderColor: 'gray',
        padding: 10,
        marginBottom: 10,
    },
});
