import React from 'react';
import {Text, TextInput, StyleSheet} from 'react-native';
  
const InputUser = ({label, placeholder, onChageText, value,namaState,keyType}) => {
        return (
        <> 
        <Text style={styles.label}>{label} </Text>
        <TextInput placeholder={placeholder}
        secureTextEntry={true} style={styles.default} value= "abc"
        style={styles.inputarea}
        keyboardType={keyType}
        onChangeText={text => onChageText(namaState, text)} 
        value={value} />
        </>
    );
};
export default InputUser;

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        marginBottom:5,
    },
    inputarea: {
        textAlignVertical: 'top',
        borderColor: 'gray',
        padding: 10,
        marginBottom: 10,
        backgroundColor: 'white',
        width:300,
        borderRadius: 25,
        paddingHorizontal:16,
        fontSize:16,
    },
});
