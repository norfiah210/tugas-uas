import firebase from 'firebase';
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyCNhh47c0_2I8mo5jPy95mQywMBlOCENFs",
    authDomain: "simdes-lesday.firebaseapp.com",
    databaseURL: "https://simdes-lesday-default-rtdb.firebaseio.com",
    projectId: "simdes-lesday",
    storageBucket: "simdes-lesday.appspot.com",
    messagingSenderId: "295828637007",
    appId: "1:295828637007:web:b187b7f377e91552c8b13a",
    measurementId: "G-KRH0L7RWDL"
  };

  firebase.initializeApp(firebaseConfig);
  var FIREBASE = firebase;
  export default FIREBASE;
