import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, Alert, ScrollView} from 'react-native';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import FIREBASE from '../../config/FIREBASE';
import CardMember from '../../component/CardMember';

export default class Anggota extends Component {
    constructor(props) {
        super(props)

        this.state = {
            members: {},
            membersKey: []
        }
    }
    componentDidMount() {
       this.ambilData();
    }

    ambilData = () => {
        FIREBASE.database()
       .ref("Member")
       .once('value', (querySnapShot) => {
           let data = querySnapShot.val() ? querySnapShot.val() : {};
           let anggota = {...data};

           this.setState({
               members: anggota,
               membersKey: Object.keys(anggota)
           })
       })
    }

    removeData = (id) =>{
        Alert.alert(
            "Warning",
            " Apakah anda yakin akan menghapus anggota ini ?",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "OK", onPress: () => {
                  FIREBASE.database()
                  .ref('Member/'+id)
                  .remove();
                  this.ambilData();
                  Alert.alert('Hapus', 'Sukses Hapus Data')
              } }
            ]
          );
    }

    render() {
        const {members, membersKey} = this.state;
        return (
            <View style={styles.via}>
                <View style={styles.navbar}>
                <Image source={require('../../assets/lbpd.jpg')} style={{width:60,height:60}} ></Image>
                <Text style={{fontWeight:'bold',fontSize:19, fontFamily:'times new roman', textAlign:'center'}} >Anggota BPD </Text>
                 </View>
                    
                 <View style={styles.listmember}>
                     <ScrollView>
                     {membersKey. length > 0 ? (
                         membersKey.map((key) => (
                             
                            <CardMember key={key} anggota={members[key]} id={key}
                            {...this.props} removeData={this.removeData}/>
                         ))
                     ):(
                         <Text>Anggota Tidak Ada</Text>
                     )}</ScrollView>
                 </View>
                <View style={styles.vbutton}>
                    <TouchableOpacity style={styles.btnAdd}
                    onPress={() => this.props.navigation.replace('AddMember')}>
                        <FontAwesomeIcon icon={faPlus} size={20} color={'white'}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
};

const styles = StyleSheet.create({
    via: {
        flex: 1,
    },
    vbutton:{
        flex : 1,
        position: 'absolute',
        bottom : 0,
        right: 0,
        margin: 30,
    },
    navbar:{
        height: 100,
        backgroundColor: 'white',
        elevation: 2,
        paddingHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
       
    },
    btnAdd: {
        padding : 20,
        backgroundColor : 'black',
        borderRadius : 30,
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation: 5,
    },
    listmember:{
        paddingHorizontal: 30,
        marginTop:20,
        backgroundColor:'wheat',
        flex:1,
    },
});