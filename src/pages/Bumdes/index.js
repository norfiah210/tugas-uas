import React, {Component} from "react";
import { StyleSheet, Text,  View, Image, ScrollView } from "react-native";
import Video from "react-native-video";

class Bumdes extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
    render() {
        return (
            <View style={styles.container}>
              <View style={styles.navbar} >
                <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman', textAlign:'center'}} >Usaha Milik Desa Lesong Daya</Text>
                </View>
                <View style={styles.conten}>
                    <ScrollView>
                <Text style={{fontFamily:'times new roman',fontSize:16,color:'white',marginLeft:5}} >1. Produk Terasi</Text>
                <Image source={require('../../assets/terasi.jpg')} style={{width: 380, height:330,marginTop: 10,marginLeft: 15}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:16,color:'white',marginLeft:5,marginTop:10}} >2. Wisata Bahari Lesong</Text>
                <Image source={require('../../assets/wlb.jpg')} style={{width: 380, height:330,marginTop: 10,marginLeft: 15}} ></Image>
                <Video source={require('../../assets/wisata.mp4')} style={{width: 550, height:500,marginTop: 10,marginLeft: 15}} ></Video>
                
                </ScrollView>
                </View>
                  </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          navbar:{
              height: 60,
              backgroundColor: 'white',
              elevation: 3,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
          },
            conten:{
              height: 60,
              backgroundColor: 'black',
              flex: 1,
              paddingTop: 10,
          },
           
}); 

export default Bumdes;