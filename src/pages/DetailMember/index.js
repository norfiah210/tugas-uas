import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import FIREBASE from '../../config/FIREBASE';

export default class DetailMember extends Component {
    constructor(props) {
        super(props)

        this.state = {
            member: {}
        }
    }
    componentDidMount() {
        FIREBASE.database()
        .ref("Member/"+ this.props.route.params.id)
        .once('value', (querySnapShot) => {
            let data = querySnapShot.val() ? querySnapShot.val() : {};
            let anggota = {...data};
 
            this.setState({
                member: anggota,               
            })
        })
     }
    render() {
        const {member} = this.state
        return (
            <View style={styles.page}>
                <Text> Nama : </Text>
                <Text style={styles.text}>{member.nama} </Text>
                <Text> Jabatan : </Text>
                <Text style={styles.text}>{member.profesi} </Text>
                <Text> Alamat : </Text>
                <Text style={styles.text}>{member.alamat} </Text>
                <Text> Masa Bhakti : </Text>
                <Text style={styles.text}>{member.masa_bakti} </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    page:{
        margin: 30,
        padding: 20,
        backgroundColor:'white',
        shadowColor : "#000",
        shadowOffset : { width : 0, height : 2 },
        shadowOpacity : 0.25,
        shadowRadius : 3.84,
        elevation: 5,
    },
    text:{
        fontWeight:'bold',
        fontSize:16,
        marginBottom:10,
        marginLeft:5,
    },
});
