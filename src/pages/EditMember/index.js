import React, {Component} from 'react';
import {View, Text, Alert, StyleSheet, TouchableOpacity} from 'react-native';
import {InputMember} from '../../component';
import FIREBASE from '../../config/FIREBASE';

export default class EditMember extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nama: '',
            jabatan:'',
            alamat:'',
            masa_bakti: '',
        };
    }
    componentDidMount() {
        FIREBASE.database()
        .ref("Member/"+ this.props.route.params.id)
        .once('value', (querySnapShot) => {
            let data = querySnapShot.val() ? querySnapShot.val() : {};
            let anggota = {...data};
 
            this.setState({
                nama: anggota.nama,
                jabatan:  anggota.profesi,
                alamat:  anggota.alamat,  
                masa_bakti:  anggota.masa_bakti,           
            })
        })
     }

    onChangeText = (namaState, value) => {
        this.setState({
            [namaState] : value
        });
    };
    onSubmit = () => {
        if(this.state.nama && this.state.jabatan && this.state.alamat && this.state.masa_bakti){
            const memberreferensi = FIREBASE.database().ref("Member/"+ this.props.route.params.id);
            const member = {
                nama: this.state.nama,
                profesi: this.state.jabatan,
                alamat: this.state.alamat,
                masa_bakti: this.state.masa_bakti
            }
            memberreferensi
                .update(member)
                .then((data) => {
                    Alert.alert('Sukses', 'Data Terupdate');
                    this.props.navigation.replace('Anggota');
                })
                .catch((error) => {
                    console.log("Error : ", error);
                })
        }else {
            Alert.alert('Erorr', 'Nama, Jabatan dan Alamat wajib di isi!');
        }
    };

    render() {
        return (
            <View style={styles.page}>
                <InputMember
                label="Nama :"
                placeholder="Masukkan Nama"
                onChageText={this.onChangeText}
                value={this.state.nama}
                namaState="nama"
                />
                <InputMember
                label="Jabatan :"
                placeholder="Masukkan Jabatan"
                isTextArea={true}
                onChageText={this.onChangeText}
                value={this.state.jabatan}
                namaState="jabatan"
                />
                <InputMember
                label="Alamat :"
                placeholder="Masukkan Alamat"
                isTextArea={true}
                onChageText={this.onChangeText}
                value={this.state.alamat}
                namaState="alamat"
                />
                <InputMember
                label="Masa Bakti :"
                placeholder="Masa Jabatan"
                onChageText={this.onChangeText}
                value={this.state.masa_bakti}
                namaState="masa_bakti"
                />
                <TouchableOpacity style={styles.button} onPress={() => this.onSubmit()}>
                    <Text style={styles.textbutton}>SIMPAN</Text>
                </TouchableOpacity>
            </View>
        );
    };
} ;

const styles = StyleSheet.create({
    page: {
        flex: 1,
        padding: 30,
    },
    button: {
        backgroundColor: 'black',
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
    },
    textbutton: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16,
    },
});