import React, {Component} from "react";
import { StyleSheet, Text,  View, ImageBackground, TouchableOpacity,Image } from "react-native";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
    render() {
        return (
            <View style={styles.container}>
              <View style={styles.navbar} >
              <TouchableOpacity style={styles.btnAdd}
                    onPress={() => this.props.navigation.navigate('Menu')}>
                        <FontAwesomeIcon icon={faBars} size={20} color={'white'}/>
               </TouchableOpacity>
                <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman', textAlign:'center',color:'white'}} >SIMDES Lesong Daya</Text>
                </View>
                <View style={styles.conten}>
                   <View style={styles.via}>
                       <ImageBackground source={require('../../assets/ikon.jpg')} style={{width:390,height:200}}>
                           <Text style={{fontWeight:'bold',fontSize:12,fontFamily:'times new roman',marginLeft:5,marginTop:10 }}>KOTA HEBAT</Text>
                           <Text style={{fontWeight:'bold',fontSize:12,fontFamily:'times new roman',marginLeft:5 }}>DIMULAI DARI</Text>
                           <Text style={{fontWeight:'bold',fontSize:12,fontFamily:'times new roman',marginLeft:5 }}>DESA HEBAT</Text>
                       </ImageBackground>
                   </View>
                   <Text style={{fontWeight:'bold',fontSize:22, textAlign:'center',marginTop:20}}>Desa Lesong Daya</Text>
                   <Text style={{fontWeight:'bold',fontSize:16, textAlign:'center'}}>Kecamatan Batumarmar Kabupaten Pamekasan</Text>
                   <Text style={{fontSize:14, textAlign:'center',}}>Jumlah Penduduk 3.823 Jiwa</Text>
                   <Image source={require('../../assets/kades.png')} style={{width:150,height:200,marginTop:15,marginHorizontal:120,borderRadius:10,borderColor:'black',borderWidth:3}}></Image>
                   <Text style={{fontSize:14, textAlign:'center',}}>Arief Budiatno</Text>
                </View>
                <View style={styles.header}>
                 <TouchableOpacity><Image source={require('../../assets/instagram.png')} style={{width: 25, height: 25,marginLeft:15}}/>
                   </TouchableOpacity><Text style={{color:'white',fontFamily:'times new roman',marginLeft:5}}>@lesongdaya_hebat</Text>
                 <TouchableOpacity><Image source={require('../../assets/fb.png')} style={{width: 40, height: 45,marginLeft:50}}/>
                 </TouchableOpacity><Text style={{color:'white',fontFamily:'times new roman'}} >@lesongdaya_hebat</Text>
                 
               </View>
                  </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          navbar:{
              height: 60,
              backgroundColor: 'black',
              elevation: 3,
              flexDirection: 'row',
              alignItems: 'center',
              
          },
            conten:{
              height: 60,
              backgroundColor: 'white',
              flex: 1,
              marginHorizontal:10,
              paddingTop: 10,
          },
          btnAdd:{
            padding : 20,
            backgroundColor : 'black',
          },         
          header:{
            backgroundColor: 'black',
            height: 60,
            borderTopWidth: 0.5,
            borderColor: '#E5E5E5',
            flexDirection: 'row',
            alignItems: 'center',
            
          },
           
}); 

export default Home;