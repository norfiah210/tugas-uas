import React, {Component} from "react";
import { StyleSheet, Text, View, Image,TouchableOpacity,  Dimensions } from "react-native";
import { FlatList } from "react-native-gesture-handler";

const numColumns=2;
const WIDTH = Dimensions.get('window').width;

class karang extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fia : [
        {menu:<Image source={require('../../assets/ketua.jpg')} style={{width:200, height:170,}} />,nomor:1},
        {menu:<Image source={require('../../assets/sekretaris.jpg')} style={{width:200, height:170,}} />,nomor:2}, 
        {menu:<Image source={require('../../assets/bendahara.jpg')} style={{width:200, height:170,}} />,nomor:3}, 
        {menu:<Image source={require('../../assets/anggota1.jpg')} style={{width:200, height:170,}} />,nomor:4},
        {menu:<Image source={require('../../assets/anggota2.jpg')} style={{width:200, height:170,}} />,nomor:5},
        {menu:<Image source={require('../../assets/anggota3.jpg')} style={{width:200, height:170,}} />,nomor:6},
        {menu:<Image source={require('../../assets/anggota4.jpg')} style={{width:200, height:170,}} />,nomor:7},
        {menu:<Image source={require('../../assets/anggota5.jpg')} style={{width:200, height:170,}} />,nomor:8},
        {menu:<Image source={require('../../assets/anggota6.jpg')} style={{width:200, height:170,}} />,nomor:9},
        {menu:<Image source={require('../../assets/anggota7.jpg')} style={{width:200, height:170,}} />,nomor:10},
        {menu:<Image source={require('../../assets/anggota8.jpg')} style={{width:200, height:170,}} />,nomor:11},
      ],
    };
  }
    render() {
        return (
            <View style={styles.container}>
              <View style={styles.navbar} >
              <Image source={require('../../assets/logokt.png')} style={{width:60,height:60}} ></Image>
                <Text style={{fontWeight:'bold',fontSize:19, fontFamily:'times new roman', textAlign:'center'}} >Anggota Karang Taruna </Text>
                 </View>
                    <View style={styles.content} >
                      <FlatList
                      data = {this.state.fia}
                      renderItem={({item, index}) => (
                        <View style={styles.size} >
                          {item.menu}
                        </View>
                      )}
                      keyExtractor={(item)=> item.nomor}
                      numColumns = {numColumns}
                      />
                      
                      </View>    
                </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          navbar:{
              height: 150,
              backgroundColor: 'white',
              elevation: 2,
              paddingHorizontal: 5,
              alignItems: 'center',
              justifyContent: 'center',
             
          },
          content:{
              
              backgroundColor: 'white',
              flex: 1,
              flexDirection:'row',
          },
          size: {
            height: WIDTH/(numColumns * 2),
            borderRadius:5,
            justifyContent:'center',
            alignItems:'center',
            marginBottom:50,
            marginTop: 50,
            backgroundColor:'white',
          },
}); 

export default karang;