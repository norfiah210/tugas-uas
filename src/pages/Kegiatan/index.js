import React, {Component} from "react";
import { StyleSheet, Text,  View, Image, ScrollView } from "react-native";

class Kegiatan extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
    render() {
        return (
            <View style={styles.container}>
              <View style={styles.navbar} >
                <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman', textAlign:'center',color:'white'}} >Kegiatan Desa Lesong Daya</Text>
                </View>
                <View style={styles.conten}>
                    <ScrollView>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}}>1. Kegiatan vaksinasi perangkat desa</Text>
                <Image source={require('../../assets/vaksin.jpg')} style={{width: 390, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}}>2. Kegiatan vaksinasi perangkat desa</Text>
                <Image source={require('../../assets/pkk.jpg')} style={{width: 390, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}}>3. Kegiatan Turnamen Batumamar United Club 2019</Text>
                <Image source={require('../../assets/bola.jpg')} style={{width: 390, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}}>4. Kegiatan Penanganan Covid-19</Text>
                <Image source={require('../../assets/ppkm.jpg')} style={{width: 390, height:350,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >5. Kegiatan Pembentukan anggota BPD</Text>
                <Image source={require('../../assets/komitmen.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >6. Kegiatan Musyawarah desa untuk APBDes </Text>
                <Image source={require('../../assets/musdes.jpg')} style={{width: 390, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >7. Kegiatan Musyawarah desa untuk Penerima BLT</Text>
                <Image source={require('../../assets/blt.jpg')} style={{width: 390, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >8. Kegiatan penyemprotan disinfektan</Text>
                <Image source={require('../../assets/covid.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >9. Kegiatan Musyawarah pengisian anggota BPD</Text>
                <Image source={require('../../assets/bpd.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >10. Kegiatan pembagian sembako</Text>
                <Image source={require('../../assets/sembako.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Image source={require('../../assets/bagi.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >11. Kegiatan musyawarah Pembentukan panitia BPD</Text>
                <Image source={require('../../assets/acara.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >12. Kegiatan musyawarah Pembentukan panitia PILKADES</Text>
                <Image source={require('../../assets/musdes2.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >11. Kegiatan Sosialisasi Tata Tertib PILKADES </Text>
                <Image source={require('../../assets/sosial.jpg')} style={{width: 390, height:370,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                <Text style={{fontFamily:'times new roman',fontSize:14,color:'white',marginTop:5,marginBottom:5,marginLeft:5}} >11. Kegiatan Vaksinasi rakyat Lesong Daya</Text>
                <Image source={require('../../assets/vaksin1.jpg')} style={{width: 390, height:330,marginTop: 5,marginLeft: 10,borderRadius:15,marginBottom:5}} ></Image>
                </ScrollView>
                </View>
                  </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          navbar:{
              height: 60,
              backgroundColor: 'black',
              elevation: 3,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
          },
            conten:{
              height: 60,
              backgroundColor: 'dimgray',
              flex: 1,
          },
           
}); 

export default Kegiatan;