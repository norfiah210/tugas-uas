import React, {Component} from "react";
import { StyleSheet, Text, View, Image, ImageBackground,TouchableOpacity, Alert } from "react-native";
import { InputUser } from "../../component";
import FIREBASE from "../../config/FIREBASE";
import { AsyncStorage } from 'react-native';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      UserName: '',
      Password: '',
    };
  }
  onChangeText = (namaState, value) => {
    this.setState({
      [namaState] : value
    });
  };
  onSubmit = () => {
    if(this.state.UserName && this.state.Password){
      const loginreferensi = FIREBASE.database().ref('Login');
      const login = {
        username: this.state.UserName,
        password: this.state.Password
      }
      loginreferensi
      .push(login)
      .then((data) => {
        Alert.alert('Sukses', 'Login Berhasil');
        this.props.navigation.replace('Home');
      })
      .catch((error) => {
        console.log("Error : ", error);
      })
    }else{
      Alert.alert('Error', 'Username dan Password wajib di isi');
    }
  };
 render() {
        return (
            <View style={styles.container}>
             <ImageBackground 
                source={require('/Users/ASUS/SIMDES/src/assets/ikon.jpg')} 
                style={{flex:1, ResizeMode:'screen', justifyContent: 'center', alignItems: 'center',
                      }}
                >
                  <View style={styles.page}>
                    <Image source={require('../../assets/simdes.jpg')} style={{width:350,height:200,marginTop:10}}></Image>
                    <InputUser
                    placeholder={'UserName'}
                    keyType="email-address"
                    onChageText={this.onChangeText}
                    value={this.state.UserName}
                    namaState="UserName"
                    />
                   <InputUser
                    placeholder={'Password'}
                    onChageText={this.onChangeText}
                    value={this.state.Password}
                    namaState="Password"
                    />
                  <TouchableOpacity style={{borderColor: 'black',
                    width: 100,height:50,
                    paddingHorizontal: 10,
                    borderRadius: 10,
                    marginTop: 15,
                    backgroundColor: 'white',
                    color: 'black',}} onPress={() => this.onSubmit()}>
                    <Text style={{textAlign:'center',fontFamily:'times new roman',fontSize:20,color:'black',marginTop:9}} >Login</Text></TouchableOpacity>
                    </View>
                  </ImageBackground>
              </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          page:{
            borderColor: 'black',
            width: 380,height:470,
            paddingHorizontal: 10,
            borderRadius: 15,
            backgroundColor: 'gainsboro',
            
            alignItems:'center',
          },
          inputBox: {
               width:300,
               borderRadius: 25,
               paddingHorizontal:16,
               fontSize:16,
               backgroundColor: 'white',
               color:'black',
               marginVertical: 10
              },
      }); 

export default Login;