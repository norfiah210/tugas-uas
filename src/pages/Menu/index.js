import React, {Component} from "react";
import { StyleSheet, Text, View, Image,TouchableOpacity,  Dimensions } from "react-native";
import { FlatList } from "react-native-gesture-handler";

const numColumns=2;
const WIDTH = Dimensions.get('window').width;

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fia : [
        {menu: <TouchableOpacity onPress ={() => this.props.navigation.navigate('Profile')} >
          <Image source={require('../../assets/Profil.png')} style={{width:200, height:150,}} /></TouchableOpacity>,nomor:1},
        {menu: <TouchableOpacity onPress ={() => this.props.navigation.navigate('Struktur')} >
        <Image source={require('../../assets/desa.png')} style={{width:200, height:150,}} /></TouchableOpacity>,nomor:2}, 
        {menu: <TouchableOpacity onPress ={() => this.props.navigation.navigate('Kegiatan')} >
        <Image source={require('../../assets/kegiatan.png')} style={{width:200, height:150,}} /></TouchableOpacity>,nomor:3}, 
        {menu: <TouchableOpacity onPress ={() => this.props.navigation.navigate('Anggota')} >
        <Image source={require('../../assets/anggota.png')} style={{width:200, height:150,}} /></TouchableOpacity>,nomor:4},
        {menu: <TouchableOpacity onPress ={() => this.props.navigation.navigate('Bumdes')} >
        <Image source={require('../../assets/bumdes.png')} style={{width:200, height:150,}} /></TouchableOpacity>,nomor:5},
        {menu: <TouchableOpacity onPress ={() => this.props.navigation.navigate('Karang')} >
        <Image source={require('../../assets/karang.png')} style={{width:200, height:150,}} /></TouchableOpacity>,nomor:6},
      ],
    };
  }

    render() {
        return (
            <View style={styles.container}>
              <View style={styles.navbar} >
              <Image source={require('../../assets/logo.jpg')} style={{width:60,height:60}} ></Image>
                <Text style={{fontWeight:'bold',fontSize:16, fontFamily:'times new roman', textAlign:'center'}} >Selamat Datang di Desa </Text>
                <Text style={{fontWeight:'bold',fontSize:16, fontFamily:'times new roman', textAlign:'center'}} > Lesong Daya </Text>
                <Text style={{fontWeight:'bold',fontSize:16, fontFamily:'times new roman',textAlign:'center'}} > kec. Batumarmar Kab. Pamekasan</Text>
                </View>
                    <View style={styles.content} >
                      <FlatList
                      data = {this.state.fia}
                      renderItem={({item, index}) => (
                        <View key={index} style={styles.size} >
                          {item.menu}
                        </View>
                      )}
                      keyExtractor={(item)=> item.nomor}
                      numColumns = {numColumns}
                      />
                      
                      </View> 
                    </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          navbar:{
              height: 150,
              backgroundColor: 'white',
              elevation: 2,
              paddingHorizontal: 5,
              alignItems: 'center',
              justifyContent: 'center',
             
          },
          content:{
              height: 60,
              backgroundColor: 'black',
              flex: 1,
              flexDirection:'row',
          },
          size: {
            height: WIDTH/(numColumns * 2),
            borderRadius:5,
            justifyContent:'center',
            alignItems:'center',
            marginBottom:50,
            marginTop: 50,
          },
}); 

export default Menu;