import React, {Component} from "react";
import { StyleSheet, Text, View, Image, ImageBackground } from "react-native";
import { FlatList } from "react-native-gesture-handler";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fia :[
        {teks:<Text style={{fontFamily:'times new roman', fontSize: 17,marginLeft:4,color:'white'}} >A. Profil Desa Lesong Daya</Text>,nomor:1},
        {teks:<Text style={{fontFamily: 'times new roman', fontSize: 15,marginLeft:25,textAlign:'justify',marginRight:5,color:'white'}} >Desa lesong daya merupakan salah satu desa yang termasuk dalam organisasi kecamatan batumarmar 
        yang terletak di pantai utara pulau Madura menurut letak geografisnya. Desa lesong daya tersebut terletak jauh dari hiruk-pikuk perkotaan namun tidak menjadikannya sebagai 
        daerah yang tertinggal. Sebuah desa kecil yang termasuk kawasan kabupaten pamekasan. Dengan memiliki jumlah penduduk 3.823 orang dengan rincian 1.811 laki-laki dan 2.012 
        perempuan, dengan memiliki luas 6,01 km2 , desa tersebut memiliki 5 dusun di antaranya: Dusun Sangoleng, Dusun Brumbung, Dusun Jurang Butoh, Dusun Tobelle dan Dusun Tenga. </Text>,nomor:2},
        {teks:<Text style={{fontFamily:'times new roman', fontSize: 17,marginLeft:4,color:'white'}} >B. Sejarah Pemerintahan Desa </Text>,nomor:3},
        {teks:<Text style={{fontFamily: 'times new roman', fontSize: 15,marginLeft:25,textAlign:'justify',marginRight:5,color:'white'}} >Sejarah pemerintahan desa lesong daya tidak terlepas dari sejarah masyarakat, konon menurut cerita
        para sesepuh desa pada zaman kerajaan yang ada di kampung ini yaitu ada kaitannya dengan kerajaan pamekasan, kerajaan sampang, kerajaan sumenep dan kerajaan bangkalan dalam menghadapi
        penjajah pada kala itu keempat kerajaan tersebut bergabung untuk mengadakan musyawarah untuk mengatur strategi peperangan, pertahanan dan perdagangan yang bertempat di desa lesong daya.
        Desa lesong daya merupakan desa yang di pimpin pertama kali oleh bapak Singo laut, Syafiuddin, Asma’uddin, Muhari, Matsudin dan Arief budiatno yang memimpin pemerintahan desa hingga saat ini.</Text>,nomor:4},
        {teks: <Text style={{fontFamily:'times new roman', fontSize: 17,marginLeft:4,color:'white'}} >C. Kondisi Geografis Desa </Text>,nomor:5},
        {teks:<Text style={{fontFamily: 'times new roman', fontSize: 15,marginLeft:25,textAlign:'justify',marginRight:5,color:'white'}} >Luas wilayah desa lesong daya adalah 6,01 km2, jarak dari pemerintahan desa ke pusat pemerintahan kecamatan 7 km,
        sedangkan jarak dari pusat desa ke ibu kota kabupaten 44 km. Geografis wilayah desa lesong daya meliputi perbatasan desa:</Text>,nomor:6},
        {teks:<Text style={{fontFamily:'times new roman', fontSize: 15, marginLeft:30,color:'white'}} >1. Sebelah utara berbatasan dengan laut jawa </Text>,nomor:7},
        {teks:<Text style={{fontFamily:'times new roman', fontSize: 15,marginLeft:30,textAlign:'justify',marginRight:5,color:'white'}} >2.	Sebelah timur berbatasan dengan desa kapong dan desa ponjanan barat </Text>,nomor:8},
        {teks: <Text style={{fontFamily:'times new roman', fontSize: 15,marginLeft:30,color:'white'}} >3.	Sebelah selatan berbatasan dengan desa lesong laok </Text>,nomor:9},
        {teks:<Text style={{fontFamily:'times new roman', fontSize: 15,marginLeft:30,color:'white'}} >4.	Sebelah barat berbatasan dengan desa batubintang  </Text>,nomor:10},
        {teks:<Text style={{fontFamily: 'times new roman', fontSize: 15,marginLeft:25,textAlign:'justify',marginRight:5,color:'white'}} >Mengingat letak desa lesong daya dalam pengelolaan pembangunan merupakan pertemuan antara pelaksanaan pembangunan 
        dari berbagai sector dengan pembangunan swadaya  masyarakat. Sementara itu desa lesong daya merupakan unit pemerintahan paling selatan yang berbatasan dengan desa lesong laok dalam menunjang 
        pelaksanaan tugas penyelenggaraan pemerintahan daerah. Dengan mempertimbangkan posisi desa lesong daya yang demikian strategis, maka kapasitas managemen pembangunan di desa lesong daya merupakan 
        factor yang sangat penting dalam menentukan arah kebijakan dan kesejahteraan masyarakat desa lesong daya.</Text>,nomor:11},
        {teks:<Text style={{fontFamily:'times new roman', fontSize: 17,marginLeft:4,color:'white'}} >D. Demografis/Kependudukan Desa </Text>,nomor:12},
        {teks:<Text style={{fontFamily: 'times new roman', fontSize: 15,marginLeft:25,textAlign:'justify',marginRight:5,color:'white'}} >Berdasarkan data administrasi pemerintahan desa, jumlah penduduk yang tercatat secara administrasi, jumlah total 3.823 jiwa
        dari 872 kepala keluarga. Dengan rincian penduduk berjenis kelamin laki-laki berjumlah 1.811 jiwa, sedangkan yang berjenis kelamin perempuan berjumlah 2.012 jiwa. Survey data sekunder dilakukan oleh
        kader pembangunan desa, di maksudkan sebagai data pembanding dari data yanf ada di pemerintahan desa.</Text>,nomor:13},

      ]
    };
  }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navbar} >
                  <Image source={require('../../assets/logo.jpg')} style={{width:50,height:50}} ></Image>
                  <Text style={{fontWeight:'bold',fontSize:20, fontFamily:'times new roman',marginLeft:10}} >Profile Desa Lesong Daya</Text>
                </View>
                    <View style={styles.content} >
                      <ImageBackground source={require('../../assets/back.jpg')} style={{resizeMode:'cover'}} >
                         <FlatList
                        data = {this.state.fia}
                        renderItem={({item, index})=> (
                          <View style={{marginBottom:10}}>
                            {item.teks}
                          </View>
                        )}
                        keyExtractor={(item)=> item.nomor}
                        />
                      </ImageBackground>
                       
                    </View>    
            </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          navbar:{
              height: 60,
              backgroundColor: 'white',
              elevation: 3,
              paddingHorizontal: 15,
              flexDirection: 'row',
              alignItems: 'center',
          },
          rightnav:{
             flexDirection: 'row',
          },
          navitem:{
            width: 35,
            height: 35,
            marginLeft: 25,
          },
         notif:{
              fontSize: 12,
              color: 'white',
              backgroundColor: 'red',
              width: 19,
              height: 19,
              borderRadius: 19/2,
              position: 'relative',
              top: 5,
              right: 0,
            },
            content:{
             
              flex: 1,
          },
}); 

export default Profile;