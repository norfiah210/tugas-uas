import React, {useEffect} from 'react';
import {StyleSheet, Image, View, Text} from 'react-native';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Login');
        }, 3000);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={require('../../assets/splash.jpg')} style={{width:350, height:200, justifyContent:'center',}} ></Image>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    foto:{
        margin: 10,
    },
    welcomeText:{
        color: 'maroon',
        fontSize: 24,
        fontWeight: 'bold',
        paddingBottom: 20,
    },
    kata:{
        color: 'black',
        fontSize: 14,
        fontWeight: 'bold',
        paddingBottom: 40,
        marginLeft: 200,
    },
});

export default Splash;