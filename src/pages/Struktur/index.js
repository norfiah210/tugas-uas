import React, {Component} from "react";
import { StyleSheet, Text,  View, Image, TouchableOpacity, ScrollView } from "react-native";

class Struktur extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
    render() {
        return (
            <View style={styles.container}>
              <View style={styles.navbar} >
                <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman',color: 'white',textAlign:'center',marginLeft:50}} >Struktur Desa Lesong Daya</Text>
                </View>
                <View style={styles.conten}>
                <Image source={require('../../assets/struktur.jpg')} style={{width:410,height:240,marginTop:10}} ></Image>
                <Text style={{fontWeight:'bold',fontSize:20,fontFamily:'times new roman',color:'black',textAlign:'center',paddingTop:5}} >Biodata Perangkat Desa</Text>
                <ScrollView>
                <View style={styles.bio}>
                  <Image source={require('../../assets/arif.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Arief Budiatno</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 22 Desember 1978</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Sangoleng Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Desa </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/syukur.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : ABD Syukur</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 16 Maret 1989</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Sangoleng Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Sekretaris Desa </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/nur.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama    : Nor Hadi</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala   : 18 Januari 1978</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat : Dsn. Tenga Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan: Kepala Pelayanan </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/asmin.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Asmin </Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 01 Juli 1987</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Brumbung Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kesra  </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/zaini.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Ahmad Zaini</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 18 Juni 1986</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Sangoleng Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Pemerintahan </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/samo.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Samo. S.Ag</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 25 Mei 1983</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Sangoleng Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Tatata Usaha </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/bahol.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Misbahul Munir</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 04 April 1996</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Sangoleng Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Keuangan </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/ftmui.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Mu'i </Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 08 januari 1977</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Tenga Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Perencanaan </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/jamal.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Achmad Jamal</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 24 April 1993</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Tenga Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Dusun Tenga </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/affan.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Achmad Affan Fadli</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 21 Juli 1996</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Brumbung Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Dusun Brumbung </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/suhri.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Moh. Suhri</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 12 Juni 1997</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Tobelle Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Dusun Tobelle </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/nanang.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Nanang Irawan </Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 12 September 1995</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Sangoleng Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Dusun Sangoleng </Text>
                </View>
                <View style={styles.bio}>
                  <Image source={require('../../assets/samsul.jpg')} style={{ width: 100, height: 100, borderRadius:50, borderWidth: 3, borderColor: 'white', marginTop: 20,
                  marginHorizontal:150,}}></Image>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,paddingTop:10,color:'white'}}>Nama     : Samsul Arifin</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Tetala    : 15 januari 1979</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white'}}>Alamat  : Dsn. Jureng Butoh Ds. Lesong Daya</Text>
                  <Text style={{fontWeight:'bold',fontSize:16,fontFamily:'times new roman', textAlign:'justify',marginHorizontal:15,color:'white',marginBottom:10}}>Jabatan : Kepala Dusun Jureng Butoh </Text>
                </View>
                </ScrollView>
                </View>
                  </View>
          )
        }
       }
       const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column'
          },
          navbar:{
              height: 60,
              backgroundColor: 'black',
              elevation: 3,
              paddingHorizontal: 15,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
          },
          conten:{
              height: 90,
              backgroundColor: 'papayawhip',
              flex: 1,
          },
          bio:{
            marginTop:10, 
            backgroundColor: 'dimgray',
            borderRadius:10,
            marginHorizontal: 10
          }
           
}); 

export default Struktur;