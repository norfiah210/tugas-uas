import Anggota from './Anggota';
import AddMember from './AddMember';
import Menu from './Menu';
import Home from './Home';
import Bumdes from './Bumdes';
import Karang from './Karang';
import Kegiatan from './Kegiatan';
import Login from './Login';
import Profile from './Profile';
import Splash from './Splash';
import Struktur from './Struktur';
import DetailMember from './DetailMember';
import EditMember from './EditMember';

export {Anggota, AddMember, Menu,Home, Bumdes, Karang, Kegiatan, Login, Profile, Splash, Struktur,DetailMember, EditMember};