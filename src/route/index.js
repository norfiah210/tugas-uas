import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Anggota, AddMember, Splash, Login, Home, DetailMember, EditMember, } from '../pages';
import Menu from '../pages/Menu';
import Profile from '../pages/Profile';
import Struktur from '../pages/Struktur';
import Kegiatan from '../pages/Kegiatan';
import Karang from '../pages/Karang';
import Bumdes from '../pages/Bumdes';



const Stack = createStackNavigator();
const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Home"
            component={Home}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Menu"
            component={Menu}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Profile"
            component={Profile}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Struktur"
            component={Struktur}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Kegiatan"
            component={Kegiatan}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Karang"
            component={Karang}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Bumdes"
            component={Bumdes}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Anggota"
            component={Anggota}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="AddMember"
            component={AddMember}
            options={{title: 'Add Member'}}            
            />
             <Stack.Screen
            name="DetailMember"
            component={DetailMember}
            options={{title: 'Detail Member'}}
            />
            <Stack.Screen
            name="EditMember"
            component={EditMember}
            options={{title: 'Edit Member'}}
            />
        </Stack.Navigator>
    );
};

export default Route;